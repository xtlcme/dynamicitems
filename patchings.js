import {Item5e} from "/systems/dnd5e/module/item/entity.js"
import {Actor5e} from "/systems/dnd5e/module/actor/entity.js"
import {Dice5e} from "/systems/dnd5e/module/dice.js"



  /* -------------------------------------------- */
  /*  Item Rolls - Attack, Damage, Saves, Checks  */
  /* -------------------------------------------- */

  /**
   * Place an attack roll using an item (weapon, feat, spell, or equipment)
   * Rely upon the Dice5e.d20Roll logic for the core implementation
   *
   * @return {Promise.<Roll>}   A Promise which resolves to the created Roll instance
   */
   let myRollAttack = function(options={}) {
    const itemData = this.data.data;
    const actorData = this.actor.data.data;
    if ( !this.hasAttack ) {
      throw new Error("You may not place an Attack Roll with this Item.");
    }

    // Define Roll parts
    const parts = ["@atk", `@mod`, "@prof"];
    if ( (this.data.type === "weapon") && !itemData.proficient ) parts.pop();

    // Define Critical threshold
    let crit = 20;
    if ( this.data.type === "weapon" ) crit = this.actor.getFlag("dnd5e", "weaponCriticalThreshold") || 20;

    // Define Roll Data
    const rollData = this._getRollData();
    const actorBonus = actorData.bonuses[itemData.actionType] || {};
    rollData["atk"] = parseInt(itemData.attackBonus) + ` + ${actorBonus.attack}`;

    // Call the roll helper utility
    const title = `${this.name} - Attack Roll`;
    return Dice5e.d20Roll({
      event: options.event,
      parts: parts,
      actor: this.actor,
      data: rollData,
      title: title,
      speaker: ChatMessage.getSpeaker({actor: this.actor}),
      critical: crit,
      dialogOptions: {
        width: 400,
        top: options.event ? options.event.clientY - 80 : null,
        left: window.innerWidth - 710
      }
    });
  }

  /* -------------------------------------------- */

  /**
   * Place a damage roll using an item (weapon, feat, spell, or equipment)
   * Rely upon the Dice5e.damageRoll logic for the core implementation
   *
   * @return {Promise.<Roll>}   A Promise which resolves to the created Roll instance
   */
  let myRollDamage = function({event, spellLevel=null, versatile=false}={}) {
    const itemData = this.data.data;
    const actorData = this.actor.data.data;
    if ( !this.hasDamage ) {
      throw new Error("You may not make a Damage Roll with this Item.");
    }

    // Define Roll parts
    const parts = itemData.damage.parts.map(d => d[0]);
    if ( versatile && itemData.damage.versatile ) parts[0] = itemData.damage.versatile;
    if ( (this.data.type === "spell") ) {
      if ( (itemData.scaling.mode === "cantrip") ) {
        const lvl = this.actor.data.type === "character" ? actorData.details.level : actorData.details.spellLevel;
        this._scaleCantripDamage(parts, lvl, itemData.scaling.formula );
      } else if ( spellLevel && (itemData.scaling.mode === "level") && itemData.scaling.formula ) {
        this._scaleSpellDamage(parts, itemData.level, spellLevel, itemData.scaling.formula );
      }
    }

    // Define Roll Data
    const rollData = this._getRollData();
    const actorBonus = actorData.bonuses[itemData.actionType] || {};
    if ( actorBonus.damage && (actorBonus.damage !== 0) ) {
      parts.push("@dmg");
      rollData["dmg"] = `+${actorBonus.damage}`;
    }

    // Call the roll helper utility
    const title = `${this.name} - Damage Roll`;
    const flavor = this.labels.damageTypes.length ? `${title} (${this.labels.damageTypes})` : title;
    return Dice5e.damageRoll({
      event: event,
      parts: parts,
      actor: this.actor,
      data: rollData,
      title: title,
      flavor: flavor,
      speaker: ChatMessage.getSpeaker({actor: this.actor}),
      dialogOptions: {
        width: 400,
        top: event ? event.clientY - 80 : null,
        left: window.innerWidth - 710
      }
    });
  }

  /**
 * Roll a Skill Check
 * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
 * @param {string} skillId      The skill id (e.g. "ins")
 * @param {Object} options      Options which configure how the skill check is rolled
 * @return {Promise.<Roll>}   A Promise which resolves to the created Roll instance
 */
let myRollSkill = function(skillId, options={}) {
  const skl = this.data.data.skills[skillId];
  const parts = ["@mod"];
  const data = {mod: skl.mod};
console.log("In my roll skill check")
  // Include a global actor skill bonus
  const actorBonus = getProperty(this.data.data.bonuses, "skills.check");
  if ( !!actorBonus ) {
    parts.push("@skillBonus");
    data.skillBonus = `+${actorBonus}`;
  }

  // Roll and return
  return Dice5e.d20Roll({
    event: options.event,
    parts: parts,
    data: data,
    title: `${CONFIG.DND5E.skills[skillId]} Skill Check`,
    speaker: ChatMessage.getSpeaker({actor: this}),
  });
}
 /**
   * Roll an Ability Test
   * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
   * @param {String} abilityId    The ability ID (e.g. "str")
   * @param {Object} options      Options which configure how ability tests are rolled
   * @return {Promise.<Roll>}   A Promise which resolves to the created Roll instance
   */
  let myRollAbilityTest = function(abilityId, options={}) {
    const label = CONFIG.DND5E.abilities[abilityId];
    const abl = this.data.data.abilities[abilityId];
    const parts = ["@mod"];
    const data = {mod: abl.mod};

    console.log("In my roll ability test")
    // Include a global actor ability check bonus
    const actorBonus = getProperty(this.data.data.bonuses, "abilities.check");
    if ( !!actorBonus ) {
      parts.push("@checkBonus");
      data.checkBonus = `+${actorBonus}`;
    }

    // Roll and return
    return Dice5e.d20Roll({
      event: options.event,
      parts: parts,
      data: data,
      title: `${label} Ability Test`,
      speaker: ChatMessage.getSpeaker({actor: this}),
    });
  }

  /* -------------------------------------------- */

  /**
   * Roll an Ability Saving Throw
   * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
   * @param {String} abilityId    The ability ID (e.g. "str")
   * @param {Object} options      Options which configure how ability tests are rolled
   * @return {Promise.<Roll>}   A Promise which resolves to the created Roll instance
   */
  let myRollAbilitySave = function(abilityId, options={}) {
    const label = CONFIG.DND5E.abilities[abilityId];
    const abl = this.data.data.abilities[abilityId];
    const parts = ["@mod"];
    const data = {mod: abl.save};
    // Include a global actor ability save bonus
    const actorBonus = getProperty(this.data.data.bonuses, "abilities.save");
    if ( !!actorBonus ) {
      parts.push("@saveBonus");
      data.saveBonus = `+${actorBonus}`;
    }
    console.log("In my saving throw", actorBonus, options.event)

    // Roll and return
    return Dice5e.d20Roll({
      event: options.event,
      parts: parts,
      data: data,
      title: `${label} Saving Throw`,
      speaker: ChatMessage.getSpeaker({actor: this}),
    });
  }
  class DummyActor extends Actor {
      prepareData() {
        super.prepareData();
      // Get the Actor's data object
      const actorData = this.data;
      const data = actorData.data;
      const flags = actorData.flags;

      // Prepare Character data
      if ( actorData.type === "character" ) this._prepareCharacterData(actorData);
      else if ( actorData.type === "npc" ) this._prepareNPCData(actorData);

      // Ranged Weapon/Melee Weapon/Ranged Spell/Melee Spell attack bonuses are added when rolled since they are not a fixed value.
      // Damage bonus added when rolled since not a fixed value.

      // Ability modifiers and saves
      // Character All Ability Check" and All Ability Save bonuses added when rolled since not a fixed value.
      
      for (let abl of Object.values(data.abilities)) {
        abl.mod = Math.floor((abl.value - 10) / 2);
        abl.save = abl.mod + ((abl.proficient || 0) * data.attributes.prof);
      }

      // Skill modifiers
      for (let skl of Object.values(data.skills)) {
        skl.value = parseFloat(skl.value || 0);
        skl.bonus = parseInt(skl.bonus || 0);
        skl.mod = data.abilities[skl.ability].mod + skl.bonus + Math.floor(skl.value * data.attributes.prof);
        skl.passive = 10 + skl.mod;
      }

      // Initiative
      const init = data.attributes.init;
      init.mod = data.abilities.dex.mod;
      init.prof = getProperty(flags, "dnd5e.initiativeHalfProf") ? Math.floor(0.5 * data.attributes.prof) : 0;
      init.bonus = init.value + (getProperty(flags, "dnd5e.initiativeAlert") ? 5 : 0);
      init.total = init.mod + init.prof + init.bonus;

      // Spell DC
      data.attributes.spelldc = this.getSpellDC(data.attributes.spellcasting);
    }
  }

Hooks.once("init", () => {
  if (game.system.data.version === 0.83) {
    let proxy = new Proxy( Item5e.prototype.rollAttack, {
      apply: (target, thisvalue, args) => {
        myRollAttack.bind(thisvalue)(...args)
      }
    })
    Item5e.prototype.rollAttack = proxy;
  }

  if (game.system.data.version === 0.83) {
    let proxy = new Proxy( Item5e.prototype.rollDamage, {
      apply: (target, thisvalue, args) =>
        myRollDamage.bind(thisvalue)(...args)
    })
    Item5e.prototype.rollDamage = proxy;
  }

  if (game.system.data.version === 0.83) {
    let proxy = new Proxy( Actor5e.prototype.rollSkill, {
      apply: (target, thisvalue, args) =>
        myRollSkill.bind(thisvalue)(...args)
    })
    Actor5e.prototype.rollSkill = proxy;
  }
  if (game.system.data.version === 0.83) {
    let proxy = new Proxy( Actor5e.prototype.rollAbilityTest, {
      apply: (target, thisvalue, args) =>
        myRollAbilityTest.bind(thisvalue)(...args)
    })
    Actor5e.prototype.rollAbilityTest = proxy;
  }
  if (game.system.data.version === 0.83) {
    let proxy = new Proxy( Actor5e.prototype.rollAbilitySave, {
      apply: (target, thisvalue, args) =>
        myRollAbilitySave.bind(thisvalue)(...args)
    })
    Actor5e.prototype.rollAbilitySave = proxy;
  }

  if (game.system.data.version === 0.84) {
    console.log("Pathcing prepare data in patchings")
    let proxy = new Proxy( Actor5e.prototype.prepareData, {
      apply: (target, thisvalue, args) =>
        DummyActor.prototype.prepareData.bind(thisvalue)(...args)
    })
    Actor5e.prototype.prepareData = proxy;
  }
})